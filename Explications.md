# Exercice ECORP ORG CHART


## 1 - Récupération des données, informations nécessaire à la réalisation de l'exercice


Nous avons  téléchargé et importé  la structure et les données à partir du lien donné

![Alt text](images/lien.png)

Nous avons téléchargé le mld

![MLD](images/mld.png)

## 2 - Création d'un dossier sur l'ordinateur

Nous avons crée sur chaque ordinateur un dossier comprenant 3 sous-dossiers (BDD, MIGRATION,SITE_INTERNET)

![Dossier_ordi](images/dossier.png)

## 3 - Création d'un fichier sql de requête

Récupérer dansla base de données "employees" :
- id : int ; un numéro unique
- pid : int (optionel le chef chef n'a pas de chef) ; l'id de son chef / N+1 / manager.
- name : str ; le nom complet de l'employé.
- title : str ; son titre honorifique dans l'entreprise.
- salary: float ; son salaire.

```sql 
SELECT
employees.emp_no as id,
concat(employees.first_name," ",employees.last_name) as name,
dept_manager.emp_no as pid,
titles.title as title,
salaries.salary
from employees
JOIN dept_emp ON employees.emp_no = dept_emp.emp_no 
JOIN departments ON dept_emp.dept_no = departments.dept_no
JOIN dept_manager ON dept_manager.dept_no = departments.dept_no
JOIN salaries ON employees.emp_no = salaries.emp_no
JOIN titles ON employees.emp_no = titles.emp_no
WHERE dept_emp.to_date = date('9999/01/01')
    AND dept_manager.to_date = date('9999/01/01')
    AND salaries.to_date = date('9999/01/01')
    AND titles.to_date = date('9999/01/01')
    LIMIT 10
    ;
+-------+---------------------+--------+--------------+--------+
| id    | name                | pid    | title        | salary |
+-------+---------------------+--------+--------------+--------+
| 10017 | Cristinel Bouloucos | 110039 | Senior Staff |  99651 |
| 10058 | Berhard McFarlin    | 110039 | Senior Staff |  72542 |
| 10140 | Yucel Auria         | 110039 | Senior Staff |  76604 |
| 10228 | Karoline Cesareni   | 110039 | Senior Staff |  96062 |
| 10239 | Nikolaos Llado      | 110039 | Staff        |  82905 |
| 10340 | Djelloul Laventhal  | 110039 | Senior Staff |  93168 |
| 10353 | Phule Hammerschmidt | 110039 | Senior Staff |  87254 |
| 10367 | Hyuckchul Gini      | 110039 | Senior Staff |  83569 |
| 10384 | Feiyu Luft          | 110039 | Senior Staff |  78680 |
| 10418 | Candida Porotnikoff | 110039 | Staff        |  61923 |
+-------+---------------------+--------+--------------+--------+
10 rows in set, 4 warnings (0,00 sec)
```

## 4 - Création d'un fichier python pour sérialiser (convertir les données Python en format JSON)


```python
import mysql.connector
import json


# connexion à ma sql
mydb = mysql.connector.connect(
    host="localhost",
    user="nicolas",
    password="DbxsvFb56K52A7;",
    database="employees"
)
curse = mydb.cursor(dictionary=True)

 # requête pour récupérer les données
query = """ 
SELECT
employees.emp_no as id,
concat(employees.first_name," ",employees.last_name) as name,
dept_manager.emp_no as pid,
titles.title as title,
salaries.salary
from employees
JOIN dept_emp ON employees.emp_no = dept_emp.emp_no 
JOIN departments ON dept_emp.dept_no = departments.dept_no
JOIN dept_manager ON dept_manager.dept_no = departments.dept_no
JOIN salaries ON employees.emp_no = salaries.emp_no
JOIN titles ON employees.emp_no = titles.emp_no
WHERE dept_emp.to_date = date('9999/01/01')
    AND dept_manager.to_date = date('9999/01/01')
    AND salaries.to_date = date('9999/01/01')
    AND titles.to_date = date('9999/01/01')
    LIMIT 3500
    ;
"""

curse.execute(query)

# Récupération de tous les résultats
result = curse.fetchall()



# Sérialisation (Conversion des données Python en format JSON)
json_object = json.dumps(result, indent=4) 

 
# Ecriture des données dans un fichier JSON
with open("/home/nicolas/Documents/Exercices/ECORP ORG CHART/SITE_WEB/static/sample.json", "w") as outfile:
    outfile.write(json_object)

curse.close()
mydb.close()
```

## 5 - Site web - organigramme

On récupère le chemin du fichier JSON pour le coller dans le fichier 

![fichier_src](images/src.png)
![Alt text](images/config.png)

On copie le le chemin du fichier

export const dataPath = 'sample.json'

On obtient la page web suivante. 
![Page_web](images/page_web.png)

L'organigramme ne s'affiche pas complètement en raison de la quantité de données. En limitant le nonmbre de données, on ne récupère pas 
les managers.



# Fichier de migration de la BDD vers le SITE_WEB
import mysql.connector
import json

# Connection SQL: ------------------------------------------------------------------
mydb = mysql.connector.connect(host="localhost", 
                               user="antony", 
                               password="choupette", 
                               database="employees")
mycursor = mydb.cursor(dictionary=True)
# ---------------------------------------------------------------------------------+
# Méthodologie du transfert de base de donnée:                                     |
# ---------------------------------------------------------------------------------+
  # Etape 1: Requète sql:
sql = """
SELECT dept_emp.`emp_no` AS id, dept_manager.`emp_no` AS pid, 
dept_manager.`dept_no` AS departement, 
CONCAT(employees.`first_name`,' ',employees.`last_name`) as `name`,
titles.`title` AS title, salaries.`salary` AS `salary`
FROM dept_emp
JOIN dept_manager ON dept_emp.`dept_no` = dept_manager.`dept_no`
JOIN employees ON dept_emp.`emp_no` = employees.`emp_no`
JOIN salaries ON employees.`emp_no` = salaries.`emp_no`
JOIN titles ON employees.`emp_no` = titles.`emp_no`
WHERE DATE_FORMAT(dept_emp.`to_date`, "%Y") = 9999
AND DATE_FORMAT(dept_manager.`to_date`, "%Y") = 9999
AND DATE_FORMAT(salaries.`to_date`, "%Y") = 9999 
AND DATE_FORMAT(titles.`to_date`, "%Y") = 9999;
"""
mycursor.execute(sql)
list_dictionary = mycursor.fetchall()
mydb.commit()
mydb.close()

  # Etape 2: Modification des données en fonction de l'affichage du site
list_departement = []
for index, dico in enumerate(list_dictionary):
  if dico["id"] == dico["pid"]:
    del dico['pid']
  if ("pid" in dico):
    dico_format = dico
    del dico_format['departement']
    list_departement.append(dico_format)
  elif ("pid" not in dico):
    dico_format = dico
    del dico_format['departement']
    list_departement.append(dico_format)
# print(list_departement)
# print(len(list_departement))
  
  # Etape 3: Ecriture des données dans un fichier JSON
# Serializing json
json_object = json.dumps(list_dictionary, indent=4)
# Writing to sample.json
with open("/home/antony/ECORP ORG/SITE_WEB/static/sample.json", "w") as outfile:
    outfile.write(json_object)
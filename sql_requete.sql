-- [
--   { "id": 1,
--     "name": "Amber McKenzie",
--     "title": "CEO",
--     "salary": 1000.2
--   },
--   { "id": 2,
--     "pid": 1,
--     "title": "Sales manager",
--     "name": "Ava Field",
--     "salary": 500.99
--   },
--   { "id": 3,
--     "pid": 1,
--     "title": "sushi caretaker",
--     "name": "Rhys Harper",
--     "salary": 150.2
--   }
-- ]

SELECT dept_emp.`emp_no` AS id, dept_manager.`emp_no` AS pid, 
dept_manager.`dept_no` AS departement, 
CONCAT(employees.`first_name`,' ',employees.`last_name`) as `name`,
titles.`title` AS title, salaries.`salary` AS `salary`
FROM dept_emp
JOIN dept_manager ON dept_emp.`dept_no` = dept_manager.`dept_no`
JOIN employees ON dept_emp.`emp_no` = employees.`emp_no`
JOIN salaries ON employees.`emp_no` = salaries.`emp_no`
JOIN titles ON employees.`emp_no` = titles.`emp_no`
WHERE DATE_FORMAT(dept_emp.`to_date`, "%Y") = 9999
AND DATE_FORMAT(dept_manager.`to_date`, "%Y") = 9999
AND DATE_FORMAT(salaries.`to_date`, "%Y") = 9999 
AND DATE_FORMAT(titles.`to_date`, "%Y") = 9999;

-- Chaque employée, on retourne:
-- Requête ID, nom, prénom, salary avec date géré, title avec date géré
SELECT employees.`emp_no` AS id, employees.`first_name` AS prenom, employees.`last_name` AS nom, 
titles.`title` AS titre, salaries.`salary` AS salaire
FROM employees
JOIN salaries ON employees.`emp_no` = salaries.`emp_no`
JOIN titles ON employees.`emp_no` = titles.`emp_no`
WHERE DATE_FORMAT(salaries.`to_date`, "%Y") = 9999 
AND DATE_FORMAT(titles.`to_date`, "%Y") = 9999;

-- Lien des employées avec leur manager
SELECT dept_emp.emp_no AS id, dept_manager.emp_no AS pid, dept_manager.dept_no
FROM dept_emp
JOIN dept_manager ON dept_emp.dept_no = dept_manager.dept_no
WHERE DATE_FORMAT(dept_emp.to_date, "%Y") = 9999
AND DATE_FORMAT(dept_manager.to_date, "%Y") = 9999;

-- Lien des managers avec leur pid vide
SELECT emp_no AS id, case when emp_no is null then 1 else 0 end as pid,  dept_no 
FROM dept_manager
WHERE DATE_FORMAT(to_date, "%Y") = 9999;

SELECT dept_emp.`emp_no`, dept_manager.`emp_no`, dept_manager.`dept_no`, 
CONCAT(employees.`first_name`,' ',employees.`last_name`) as `name`,
titles.`title` AS title, salaries.`salary` AS `salary`
FROM dept_emp
JOIN dept_manager ON dept_emp.`dept_no` = dept_manager.`dept_no`
JOIN employees ON dept_emp.`emp_no` = employees.`emp_no`
JOIN salaries ON employees.`emp_no` = salaries.`emp_no`
JOIN titles ON employees.`emp_no` = titles.`emp_no`
WHERE DATE_FORMAT(dept_emp.`to_date`, "%Y") = 9999
AND DATE_FORMAT(dept_manager.`to_date`, "%Y") = 9999
AND DATE_FORMAT(salaries.`to_date`, "%Y") = 9999 
AND DATE_FORMAT(titles.`to_date`, "%Y") = 9999
AND dept_emp.`emp_no`= dept_manager.`emp_no`;


USE employees;

-- Gestion date employees:
SELECT * FROM employees;
SELECT emp_no, first_name, last_name FROM employees;

-- Gestion date salaries:
SELECT * FROM salaries;
SELECT emp_no, DATE_FORMAT(from_date, "%j"), DATE_FORMAT(to_date, "%j") FROM salaries;
SELECT emp_no, salary, from_date, to_date, DATEDIFF(from_date, to_date) FROM salaries;
SELECT emp_no, salary, to_date FROM salaries
WHERE DATE_FORMAT(to_date, "%Y") = 9999;
-- Conclusion: le salaire est le salaire annuel sans gestion data
SELECT emp_no, salary FROM salaries;
-- Date ???
SELECT employees.`emp_no`, employees.`first_name`, employees.`last_name`, 
salaries.`salary`, salaries.`to_date`
FROM employees
JOIN salaries ON employees.`emp_no` = salaries.`emp_no`
WHERE DATE_FORMAT(salaries.`to_date`, "%Y") = 9999
AND employees.`emp_no` = 10005;
-- Conclusion: le salaire est le salaire annuel avec gestion data
SELECT employees.`emp_no`, employees.`first_name`, employees.`last_name`, 
salaries.`salary`, salaries.`to_date`
FROM employees
JOIN salaries ON employees.`emp_no` = salaries.`emp_no`
WHERE DATE_FORMAT(salaries.`to_date`, "%Y") = 9999;

SELECT * FROM titles;
SELECT emp_no, DATE_FORMAT(from_date, "%j"), DATE_FORMAT(to_date, "%j") FROM titles;
SELECT emp_no, title, from_date, to_date, DATEDIFF(from_date, to_date) FROM titles;
SELECT emp_no, title, from_date, to_date FROM titles
WHERE DATE_FORMAT(to_date, "%Y") = 9999;


SELECT * FROM dept_emp;
SELECT emp_no, dept_no, from_date, to_date FROM dept_emp
WHERE DATE_FORMAT(to_date, "%Y") = 9999;


SELECT * FROM dept_manager;
SELECT emp_no, dept_no, from_date, to_date FROM dept_manager
WHERE DATE_FORMAT(to_date, "%Y") = 9999;

SELECT dept_emp.emp_no, dept_emp.dept_no, dept_emp.from_date, dept_emp.to_date, 
dept_manager.emp_no, dept_manager.dept_no, dept_manager.from_date, dept_manager.to_date
FROM dept_emp
JOIN dept_manager ON dept_emp.dept_no = dept_manager.dept_no
WHERE DATE_FORMAT(dept_emp.to_date, "%Y") = 9999
AND DATE_FORMAT(dept_manager.to_date, "%Y") = 9999;






SELECT
salaries.salary,
employees.emp_no as id,
concat(employees.first_name,' ',employees.last_name) as name,
dept_manager.emp_no as pid,
titles.title as title
from employees
JOIN dept_emp ON employees.emp_no = dept_emp.emp_no 
JOIN departments ON dept_emp.dept_no = departments.dept_no
JOIN dept_manager ON dept_manager.dept_no = departments.dept_no
JOIN salaries ON employees.emp_no = salaries.emp_no
JOIN titles ON employees.emp_no = titles.emp_no
WHERE dept_emp.to_date = date('9999/01/01')
    AND dept_manager.to_date = date('9999/01/01')
    AND salaries.to_date = date('9999/01/01')
    AND titles.to_date = date('9999/01/01');
    AND salary = 111133;


